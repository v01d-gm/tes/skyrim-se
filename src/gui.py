from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.scripts import ScriptUI

from injections import infinite_lp
from objects import (
    endurance,
    hp,
    invisibility,
    mana,
    max_carry_weight,
    muting_footsteps,
    weapon_attack_distance,
    weapon_attack_speed,
    weapon_damage_multiplier,
)
from scripts import better_endurance_regen, better_hp_regen, better_mana_regen


@simple_trainerbase_menu("Enderal: Forgotten Stories", 740, 460)
def run_gui():
    add_components(
        GameObjectUI(hp, "HP", set_hotkey="F1", default_setter_input_value=0),
        GameObjectUI(mana, "Mana", set_hotkey="F1", default_setter_input_value=0),
        GameObjectUI(endurance, "Endurance", set_hotkey="F1", default_setter_input_value=0),
        GameObjectUI(max_carry_weight, "Max Carry Weight"),
        GameObjectUI(invisibility, "Invisibility"),
        GameObjectUI(muting_footsteps, "Muting Footsteps"),
        SeparatorUI(),
        GameObjectUI(weapon_attack_speed, "Attack Speed"),
        GameObjectUI(weapon_attack_distance, "Attack Distance"),
        GameObjectUI(weapon_damage_multiplier, "Damage Multiplier", default_setter_input_value=1000),
        SeparatorUI(),
        ScriptUI(better_hp_regen, "Better HP Regen", "F2"),
        ScriptUI(better_mana_regen, "Better Mana Regen", "F3"),
        ScriptUI(better_endurance_regen, "Better Endurance Regen", "F4"),
        SeparatorUI(),
        CodeInjectionUI(infinite_lp, "Infinite LP", "F6"),
    )
